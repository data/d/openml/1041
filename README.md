# OpenML dataset: gina_prior2

https://www.openml.org/d/1041

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

**Note: Identical to the MNIST dataset?**

Datasets from the Agnostic Learning vs. Prior Knowledge Challenge (http://www.agnostic.inf.ethz.ch)

Dataset from: http://www.agnostic.inf.ethz.ch/datasets.php

Modified by TunedIT (converted to ARFF format)


GINA is digit recognition database

The task of GINA is handwritten digit recognition.

Data type: non-sparse
Number of features: 784
Number of examples and check-sum:
Tot_ex Check_sum
3468 90979365.00

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1041) of an [OpenML dataset](https://www.openml.org/d/1041). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1041/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1041/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1041/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

